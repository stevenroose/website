+++
title = "Projects"
+++


Like many open-source software engineers, I have started numerous projects and abandoned the
vast majority of them before they reached a usable state.
Among the projects that I do consider in a usable state, here are some highlights.


# Bitcoin

## Information

- [covenants.info](https://covenants.info): a webpage dedicated to summarizing the various different
  covenant proposals and use cases

## Software

### Tools

- [hal](https://github.com/stevenroose/hal/): a Bitcoin CLI swiss-army knife. A must-have for every
  Bitcoin developer, it helps you with decoding transactions, creating and inspecting addresses,
  working with PSBTs, signing and signature verification, BIP-39 & BIP-32 key derivation, working
  with descriptors and miniscript policies, and several other things
    
- [doubletake](https://github.com/stevenroose/doubletake): a tool (and Rust library) to create, burn
  and reclaim Bitcoin double-spend bonds, see [my blog
  post](/blog/bitcoin-double-spend-prevention-bonds-liquid) for more info


### Libraries

- [rust-bitcoin](https://github.com/rust-bitcoin/rust-bitcoin): not my creation, but I have made
  significant contributions to this awesome Bitcoin library for Rust developers. Most of my other
  projects and professional work woulnd't be possible without standing of the shoulders of the
  giants that have made this library a reality.

- [rust-bitcoincore-rpc](https://github.com/rust-bitcoin/rust-bitcoincore-rpc/): an RPC client
  library in Rust for the Bitcoin Core JSON-RPC interface

- [rust-bip39](https://github.com/stevenroose/rust-bip39): a minimal Rust BIP-39 library with zero
  heap allocations


