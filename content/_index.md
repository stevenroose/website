+++
path = "/"
title = "Latest posts"
#sort_by = "date"
template = "section.html"

[extra]
header = {title = "Steven's personal blog", img = "" }#$BASE_URL/img/profile.jpg" }
section_path = "blog/_index.md"
max_posts = 4
+++




"The ultimate, hidden truth of the world is that it is something that we make, and could just as easily make differently."
<br />— David Graeber

