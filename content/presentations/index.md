+++
title = "Presentations"
+++


## Ark

- [Ark & ecash: enemies or friends?](/presentations/ark-ecash-friends.pdf): A talk at the ecash-themed btc++
conference in Berlin.
- [Scaling Bitcoin with Ark](/presentations/ark-seoul.pdf): Ark talk at Bitcoin
  Seoul.
- [State of the Ark](/presentations/state-of-the-ark.pdf): Ark talk at
  Advancing Bitcoin in London and at Bitcoin++ in Austin.
- [Ark Intro](/presentations/ark-intro-taipei.pdf): less technical introduction
  to the Ark layer 2 protocol
- [Understanding Ark](/presentations/understanding-ark.pdf): technical
  introduction to the Ark layer 2 protocol


## TXHASH

- [TXHASH: TL;DR for those that don't read BIPs](/presentations/txhash.pdf):
  Technical summary of TXHASH proposal.
